# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org

#+TITLE: OS11: Security
#+SUBTITLE: Including parts of Chapter 11 and Section 9.6.3 of cite:Hai17
#+DATE: Computer Structures and Operating Systems 2018
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.

* Introduction

# This is the twelfth presentation for this course.
#+CALL: generate-plan(number=12)

#+TOC: headlines 1

** OS Security — So far
   - Hardware building blocks
     - Kernel mode vs user mode
       - Restrict instruction set
         - Protect kernel data structures
	 - Enable access control via system call API
     - Timer interrupts
       - Transfer control periodically back to OS
   - Major OS abstraction
     - Process
       - Virtual address spaces
	 - Isolate processes from each other
       - Access rights

** Information Security
   :PROPERTIES:
   :CUSTOM_ID: safety-security
   :END:
   - *Safety*: Protection against unintended/natural/random events
     - (Not focus here; requires proper management, involves
       training, redundancy, and insurances)
   - *Security*: Protection against deliberate attacks/threats
     - Protection of *security goals* for objects and services against
       *attackers*

*** Security Goals
    - Classical security goals: *CIA triad*
      - *Confidentiality*
	- Only intended recipient can access information
	- Typically guaranteed by encryption mechanisms
	  - (Or, e.g., with envelopes and protecting laws)
      - *Integrity*
	- Detection of unauthorized modification
	- Typically guaranteed by cryptographic checksumming mechanisms
	  - (Or, e.g., with signatures and/or seals)
      - *Availability*
	- Information and functionality available when requested
	- Supported by redundancy
      - Further goals
	- Accountability, authenticity, anonymity, (non-) deniability, …

*** Relativity
    - Security is *relative*
      - You need to *define your goals* and risks for *specific pieces*
	of information, e.g.:
	- How much confidentiality for course slides vs course exam?
	- Apparently, it’s easy to keep the slides “secure”
	  - Harder for the exam
      - Also: Who is the *attacker* with what *resources*?
	- Appropriate security mechanism, typically with *risk acceptance*
    - Security via *design process* and *management*
      - BSI (Germany) and ISO standards
	- [[https://www.bsi.bund.de/DE/Themen/ITGrundschutz/itgrundschutz_node.html][IT-Grundschutz]]
      - Topic in its own right

*** Attacker Models
    - Sample classifications of attackers
      - Strategy
	- Targeted (specialized, looks for “weakest link”)
	  - E.g., espionage, blackmailing
	- Opportunistic (standardized, looks for “weakest target”)
	  - E.g., phishing, extortion, bot/zombie creation (DDoS, spam,
            bitcoin mining, proxy)
      - Financial resources
      - Compute capacity
      - Time
      - Knowledge (insider and position?)

** Design Principles for Secure Systems
   - Selected principles based on cite:SS75
     - Fail-safe defaults (whitelisting): If no explicit permission, then deny
     - Least privilege (need to know): Subject has only those privileges that are necessary for given task
     - Economy of mechanism: Security mechanisms should be as simple as possible
     - Complete mediation: All accesses need to be checked
     - Open design: Security should not depend on secrecy; instead open reviewing
     - Separation of privilege: Permission not based on single condition
     - Psychological acceptability: Security should not hinder usage
     - And more, see cite:SS75 or cite:Hai17

** End-to-End Security
   :PROPERTIES:
   :CUSTOM_ID: end-to-end-security
   :reveal_extra_attr: data-audio-src="./audio/10-end-to-end.ogg"
   :END:
   - Security goals may have varying *scope*
     - Hop-by-hop
     - End-to-end
   - Integrity and confidentiality are *end-to-end goals*
     - Beware: That’s not generally understood!
       - (See next slide...)
     - Consider hop-by-hop confidentiality
       - Alice wants to send confidential message M to Bob via one hop, Eve
	 - Alice encrypts M for Eve, sends encrypted M to Eve
	 - Eve decrypts M, encrypts M for Bob, sends encrypted M to Bob
       - Security gain or loss? (Compared to what?)
     - Hop-by-hop integrity similarly
#+BEGIN_NOTES
Suppose that you want to send some e-mail to a friend, where the
e-mail’s contents are a private matter.  In this case, the security
goal confidentiality needs to be protected.
Quite likely, you want confidentiality as an end-to-end goal meaning
that only the communication endpoints, namely you and your friend, can read
the message, independently of the number of hops or intermediary
machines (such as Internet backbone routers) that forward the message
from you to your friend.

If you send the e-mail as usual, sender and recipient need a password
to access their accounts and e-mails at their providers’ servers.
Thus, some protection is offered for e-mails at their destinations.
However, obviously also the providers’ administrators and everybody
else with access to their infrastructures (such as intelligence
agencies violating human rights and other criminals) have access to
the e-mails.  Thus, those parties can access your draft folder as well
as the recipient’s inbox to access messages, violating
confidentiality.

Besides, in the case of e-mail it is not clear whether e-mails forwarded
between providers are encrypted or not.  In response to the Snowden
revelations there is a major shift towards encryption in transit;
however, this type of encryption is not guaranteed.  Thus, your
e-mail might also traverse the Internet in plaintext, and on its way
it typically passes a couple of computers owned by parties that are
unknown to you and that might copy or change your e-mails.
Actually, when e-mails cross country borders it’s almost certain that
intelligence agencies copy the messages, again violating
confidentiality.  Obviously, this type of confidentiality violation
can be prevented if providers encrypt their message exchanges, which
would guarantee confidentiality on a hop-by-hop basis.

Clearly, encryption on a hop-by-hop basis is better than no
protection, while you need to take protection into your own hands if
you are interested in end-to-end goals.
#+END_NOTES

*** (Counter-) Example: De-Mail
   :PROPERTIES:
   :CUSTOM_ID: de-mail
   :reveal_extra_attr: data-audio-src="./audio/10-de-mail.ogg"
   :END:
    - [[https://de.wikipedia.org/wiki/De-Mail][De-Mail]] is a German approach *defining* legally binding, “secure” e-mail
      [[./img/10-de-mail-small.png]]
    - General picture
      - [[color:darkblue][Strong]] (hop-by-hop) security for each of the
	three [[color:darkblue][blue]] links
      - [[color:darkred][Plaintext]] at both providers (and
        [[color:darkred][broken]] approach towards integrity, see cite:Lec11)
	- End-to-end encryption allowed
	- Digital signatures used in special cases
#+BEGIN_NOTES
De-Mail serves as example for hop-by-hop security and as
counter-example for end-to-end security.  Key characteristics are
shown on this slide.  While De-Mail may be attractive for legal
reasons when it allows to replace paper with digital communication,
I don’t see much value for individuals.

The broken aspect of integrity protection mentioned here is that the
technical specification for De-Mail includes a step “Metadaten setzen
und Integrität sichern” which adds a simple hash value that is later
checked in a step called “Integritätssicherung prüfen”.  As part of a
JiTT assignment you should convince yourself that such a hash value
provides no integrity protection against attackers.
#+END_NOTES

* Cryptography
  :PROPERTIES:
  :CUSTOM_ID: cryptography
  :END:

** Key Notions
   - Art of “secret writing”
   - Set of mathematical functions
     - Cryptographic *hash* functions
     - Classes of *encryption* algorithms
       - *Symmetric*, *secret key*: en- and decryption use the
         same shared secret key
       - *Asymmetric*, *public key*: participants own *pairs* of
         secret (decryption, signature creation) and public
         (encryption, signature verification) keys
       - *Hybrid*: asymmetric initialization, symmetric encryption
     - Basis for various security mechanisms
   - Performance
     - Hashing > Symmetric Enc. > Asymmetric Enc.
       - (One can hash more data per second than one can encrypt)
       - (One can encrypt more data per second symmetrically than asymmetrically)

*** Basic Assumptions
    - Fundamental Tenet of Cryptography from cite:KPS02
      - “If lots of smart people have failed to solve a problem, then it
	 probably won’t be solved (soon).”
      - The problem to solve here: Break specific crypto algorithm
	- If that did not happen for a long time, probably the
          algorithm is strong
	- (Lots of crypto algorithms come without security proof)
    #+ATTR_REVEAL: :frag appear
    - [[https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle][Kerckhoffs’ Principle]] (1883)
      - Security of crypto systems should not depend upon secrecy of en-
        and decryption functions (but on secrecy of the used keys)
      - “Open Design” principle from cite:SS75
	- Not respected in national security/military/intelligence
          settings in Germany
          - From Enigma through Libelle (approved for “Streng geheim”;
            developed by BSI, not published)
      - Opposite: Security through obscurity

*** Names
    - Alice and Bob; Charlie, Carol, Dave, ...
      - Communicate frequently
      - Value their privacy
      - Have limited trust in third parties
      - Appeared to be subversive individuals in the past
	- Growing understanding in general public
      - And, of course, politically correct names instead of “A” and “B”
    - Eve, Mallory, Trudy
      - Eavesdropper, malicious attacker, intruder

*** Notation
    - M, C: Message and ciphertext (encrypted messages)
    - K: Key (random bits, maybe with certain structure)
    - E, D: En- and decryption functions
    - K_{AB}: Secret key shared between Alice and Bob
    - K_{A-}: Alice’s private key
    - K_{A+}: Alice’s public key
    - K(M): Message M encrypted with key K (if function E
      is clear from context)
    - [M]_K: Message M signed with key K


** GnuPG
   - [[https://www.gnupg.org/][GNU Privacy Guard]]
     - Free software for (e-mail) encryption
     - Implementation of [[https://tools.ietf.org/html/rfc4880][OpenPGP standard]]
       - Secure e-mail based on hybrid cryptography
     - In addition, lots of cryptographic algorithms via command line
       - ~gpg --version~ ... gpg (GnuPG) 2.1.13 ... Öff. Schlüssel: RSA,
          ELG, DSA, ECDH, ECDSA, EDDSA Verschlü.: IDEA, 3DES, CAST5,
          BLOWFISH, AES, AES192, AES256, TWOFISH, CAMELLIA128,
          CAMELLIA192, CAMELLIA256 Hash: SHA1, RIPEMD160, SHA256, SHA384,
          SHA512, SHA224
     - Start by creating key pair: ~gpg --gen-key~

*** E-Mail Self-Defense
    - My suggestion: Try out OpenPGP
      - Create key pair, upload public key to server, send/receive
	 encrypted (possibly signed) e-mails
    - More specifically, follow [[https://emailselfdefense.fsf.org/en/][Email Self-Defense]]
      - GnuPG and Thunderbird with Enigmail plugin
      - Of course, other implementations exist
	- The choice is yours
      - Note: That guide contains instructions concerning the *e-mail
	 robot Edward*, which can reply to your encrypted (and signed)
	 e-mails

** (Cryptographic) Hash Functions
   :PROPERTIES:
   :CUSTOM_ID: hash-function
   :reveal_extra_attr: data-audio-src="./audio/10-hash-function-1.ogg"
   :END:
   - [[https://en.wikipedia.org/wiki/Hash_function][Hash function]]
     (or message digest): One way function H
     - Input: Message M (bit string of arbitrary length)
     - Output: Hash value H(M) (bit string of *fixed* length)
     - Collision: Different messages mapped to same hash value
   #+ATTR_REVEAL: :frag appear :audio ./audio/10-hash-function-2.ogg
   - *Cryptographic* hash value ≈ *digital fingerprint*
     - Collision *resistant* (different hash values for different
	messages)
     - Weak collision resistance of hash function H
       - Given message M it is computationally infeasible to generate
         M’ such that H(M) = H(M’)
	 - (Computationally infeasible means that attackers should
           not be able to create collisions due to resource limitations)
     - Strong collision resistance of hash function H
       - Computationally infeasible to generate M and M’ such that H(M)
         = H(M’)
#+BEGIN_NOTES
I suppose that you remember hash functions for fast searching.
Recall that hash collisions are to be expected.

With cryptographic hash functions, collisions are a Bad Thing since
hash values are supposed to serve as digital fingerprints.  Ideally,
each message (or document or piece of data or code) should have its
own, unique fingerprint.  When a message is changed, also its
fingerprint should change.  However, if a hash collision occurs and
two messages produce the same cryptographic hash value, the
fingerprint becomes unusable to distinguish them.

On the slide you see two versions of collision resistance.  Please take
a moment to convince yourself that the strong version implies the weak
version.
#+END_NOTES

*** On Collision Resistance
    :PROPERTIES:
    :CUSTOM_ID: collision-resistance
    :reveal_extra_attr: data-audio-src="./audio/10-collision-resistance-1.ogg"
    :END:
    - [[#digital-signatures][Later]]: Hash values are essence of
      digital signatures
      - Consider contract between Alice and Mallory
	- “Mallory buys Alice’s used car for 20,000€”
	  - Contract’s text is message M
	  - Digital signatures of Alice and Mallory created from H(M)
      - Suppose H not weakly collision resistant
	- Mallory may be able to create M’ with price of 1€ such that
	  H(M) = H(M’)
	- As H(M) = H(M’) there is no proof who signed what contract
   #+ATTR_REVEAL: :frag appear :audio ./audio/10-collision-resistance-2.ogg
    - Birthdays, collisions, and probability
      - Hash people to their birthdays (day and month, without year)
      - (a) Weak collision resistance: Anyone sharing /your/ birthday?
      - (b) Strong collision resistance: Any pair sharing /any/ birthday?
	- [[https://en.wikipedia.org/wiki/Birthday_problem][Birthday paradox]]
#+BEGIN_NOTES
The importance of weak collision resistance is best understood in the
context of digital signatures, which are used to create legally
binding digital contracts proving who signed what.  Without going into
details of digital signatures right now, it is sufficient to know that
the contract’s text is a message M and that digital signatures on M
are created from the cryptographic hash value H(M).

Suppose Alice and Mallory agree that Mallory buys Alice’s used car for
20,000€.  Both digitally sign the contract’s message M.  However,
Mallory changes his mind and does not want to buy the car any longer.

If hash function H is not weakly collision resistant, Mallory may be
able to create a second contract M’ which includes the price of 1€ for
Alice’s car such that H(M) = H(M’).  In this situation, as digital
signatures are derived from hash values, the digital signatures of
Alice and Mallory created for M are also valid for M’.  Thus, Alice
has no proof that Mallory signed M in the first place.

So: If a message M is given, nobody should be able to create a second
message M’ with the same hash value under weak collision resistance.

For strong collision resistance, nobody should be able to create any
collision at all, even if those collisions only occur for messages
that look like gibberish without practical value.

A different angle on collision resistance is provided by the following
birthday analogy.  Consider the hash function mapping each person to
his or her month and day of birth.  Essentially, there are 366
different hash values (including February 29), and a collision occurs
when two people share the same birthday.

Suppose you are in class.  When you wonder whether some of your fellow
students shares /your/ birthday, you consider weak collision
resistance.  In contrast, when you ask whether /any pair/ of students
shares the same birthday, you consider strong collision resistance.

For simplicity, ignore leap years and consider just 365 different
birthdays, all with the same probability.  I’m confident that for a
class of 30 students you can compute the probabilities of
(a) somebody sharing your birthday as well as
(b) any pair sharing a common birthday.
If you do the math for the first time, you may be surprised by the
high probability in case (b), which is known as the birthday paradox
(whose essence is the fact that the number of pairs grows
quadratically, about which you can read more at Wikipedia).
As the probability of case (b) is larger than that of case (a), it is
harder to defend against case (b).  Thus, hash functions targeting
strong collision resistance must be “stronger” than those offering
weak collision resistance.
#+END_NOTES

*** Hash Applications
    - Avoidance of plain text passwords
    - Integrity tests
    - Digital signatures

*** Hash Standards
    - MD4, MD5, SHA-1: Broken
    - SHA-1, SHA-2: Designed by NSA
      - Bruce Schneier, 2004:
	[[https://www.schneier.com/essays/archives/2004/08/cryptanalysis_of_md5.html][Algorithms from the NSA are considered a sort of alien technology: They come from a superior race with no explanations]]
      - Cryptographic hashing is extremely difficult, quote from 2006
	- “[[https://www.proper.com/lookit/hash-futures-panel-notes.html][Joux says that we do not understand what we are doing and that we do not really know what we want; there is agreement from all the panelists.]]”
      - 2017: SHA-1 *SHAttered* (deprecated by NIST in 2011)
	- [[https://shattered.io/]]
    - [[https://en.wikipedia.org/wiki/SHA-3][SHA-3]] (Keccak)
      - Winner of public competition from 2007 to 2012
      - Standard released in 2015

*** Sample Message and Fingerprints
#+INCLUDE: ./alice.txt src
    - Sample hash values with GnuPG
      - ~gpg --print-md SHA1 alice.txt~
	- alice.txt: 6FC1 F66C 598B D776 BA37 1A5C 2605 06CB 4CF9 0B89
      - ~gpg --print-md SHA256 alice.txt~
	- alice.txt: 84E500CB 388EE799 05F50557 43C5481B 08B0BF17 1A2AE843
	  F4A197AD 2BA68D2E
    - (Besides, specialized hashing tools exist, e.g., ~sha256sum~)

** JiTT Assignment on Hashing
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :CUSTOM_ID: jitt-hashing
    :END:
    Mark the correct statements in
    [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=846536][Learnweb]].
    - Hash functions map data of arbitrary size to data of fixed size.
    - Hash functions always produce integer output.
    - If messages of unbounded size are hashed cryptographically, an
      infinite amount of hash collisions is guaranteed.
    - If a hash function is weakly collision resistant, it is
      computationally infeasible to compute hash collisions.
    - If a hash function is strongly collision resistant, it is
      computationally infeasible to compute hash collisions.
    - If I download a piece of software along with its hash value
      produced by a weakly collision resistant function and if the
      downloaded software has that precise hash value, I can be
      pretty sure that I obtained the “correct” software (without
      accidental or malicious changes).

** Symmetric Encryption
   - Sender and recipient share secret key, K_{AB}
   - *Encryption* of *plaintext* message M with K_{AB} into *ciphertext* C
     - C = E(K_{AB}, M) = K_{AB} (M)
       - Bits of M and K_{AB} are mixed and shuffled using
	 reversible functions (e.g., XOR, bit shift)
       - Simplest, yet provably secure case:
         [[https://en.wikipedia.org/wiki/One-time_pad][One-time pad]]
         with XOR of *random* bit string and M
   - *Decryption* with same key K_{AB}
     - M = D(K_{AB}, E(K_{AB}, M))
     - Notice: Need to exchange secret key ahead of time
   - Typical symmetric algorithms: [[https://en.wikipedia.org/wiki/Advanced_Encryption_Standard][AES]], [[https://en.wikipedia.org/wiki/Triple_DES][3DES]]

** Intuition of Asymmetric Encryption
   :PROPERTIES:
   :CUSTOM_ID: asym-intuition
   :reveal_extra_attr: data-audio-src="./audio/10-asym-crypto-1.ogg"
   :END:
   - Participants own *key pairs*
     - Private key, e.g., K_{B-}: *secret*
     - Public key, e.g., K_{B+}: *public* / *published*
     - En- and decryption based on “hard” mathematical problems
   #+ATTR_REVEAL: :frag appear :audio ./audio/10-asym-crypto-2.ogg
   - Think of key pair as *safe/vault* with numeric *key pad*
     - Open safe = public key
       - Everybody can deposit messages and lock the safe
     - Opening combination = private key
       - Only the owner can open the safe and retrieve messages
#+BEGIN_NOTES
While symmetric encryption with shared keys, in particular the
one-time pad, may seem intuitively clear, asymmetric cryptography
requires some thought.
Every participant needs a key pair, which consists of a private key
and a public key.  As the names suggest, a private key needs to be
kept secret and must only be accessible by its owner, whereas the
public key can be published, e.g., on web servers or special key
servers.

This slide offers an analogy of public key cryptography with physical
safes, which might help to convey essential ideas: The public key of
Alice is used by others to encrypt messages to her, while she uses her
private key to decrypt them.  Similarly, she might offer opened safes
in the real world, into which messages can be placed and which can be
locked by everyone.  Only Alice is able to open the safe using its
opening combination to retrieve and read contained messages.  Thus,
the opening combination corresponds to her private key.

In class, you will experience another analogy, where participants and
their key pairs are identified by colors.  To that end, please bring
along pens with different colors, e.g., text markers.

A noteworthy challenge of asymmetric cryptography, which is mentioned
on the next slide, is the reliable distribution of public keys:
How does Bob know that he really obtained Alice’s public key and not
one created by Mallory and distributed in her name?  Or in the above
analogy: How does he make sure that he places his messages into
Alice’s safe and not into one owned by Mallory to which Mallory
attached the name tag “Alice”?  Answers to this question are provided
under the term “public key infrastructure”, and they frequently rely on
the idea that Bob needs to verify a fingerprint of Alice’s public key
through an out-of-band communication channel.  This highly relevant,
fascinating, and challenging topic is beyond the scope of this
presentation, though.
#+END_NOTES

** Asymmetric Encryption
   - Participants own *key pairs*
     - Private key, e.g., K_{B-}: *secret*
     - Public key, e.g., K_{B+}: *public* / *published*
   - *Encryption* of message for Bob with Bob’s *public key*
     - C = E(K_{B+}, M) = K_{B+} (M)
     - Notice: No secret key exchange necessary
   - *Decryption* with Bob’s *secret key*
     - D(K_{B-}, K_{B+}(M)) = K_{B-}(C) = M
     - Notice: Only Bob can do this
   - Challenge: Reliable distribution of public keys
     - Solution: Certificates in [[https://en.wikipedia.org/wiki/Public_key_infrastructure][Public Key Infrastructure]], PKI

*** Sample Asymmetric Algorithms
    - [[https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange][Diffie-Hellman Key Exchange]] (1976)
      - Used, e.g., in IPsec, SSL/TLS, [[https://www.torproject.org/][Tor]], [[https://otr.cypherpunks.ca/][OTR]]
	- [[https://tools.ietf.org/html/rfc7568][RFC 7568]], June 2015: SSLv3 MUST NOT be used
    - [[https://en.wikipedia.org/wiki/RSA_(algorithm)][RSA]] (Rivest, Shamir, Adleman 1978; Turing award 2002)
      - Most famous, PGP, GnuPG
    - ElGamal (1984)
      - Based on Diffie-Hellman
      - GnuPG and newer PGP variants
    - Elliptic curves
      - Newest class, shorter keys, used on smartcards/embedded devices
      - GnuPG

*** GnuPG: Hybrid Encryption
    - Create asymmetric key pair
      - ~gpg --gen-key~
      - Various options/alternatives
    - Encryption for Bob
      - ~gpg -e -a -r Bob file~
	- Creates ~file.asc~; more precisely:
	- Creates random secret key K_{AB}
	- Symmetric encryption of file with K_{AB}
	  - Specific algorithm obtained from Bob’s public key
	- Asymmetric encryption of K_{AB} with K_{B+}
	  - Beware! No naïve encryption, but, e.g.,
            [[https://tools.ietf.org/html/rfc8017][PKCS]] #1
	- Result: K_{B+}(K_{AB}) + K_{AB}(file)
	  - (“+” between ciphertexts denotes string concatenation)

* Message Integrity
** Situation and Goal
   - Alice sends message M to Bob
     - (Parts of) Network controlled by unknown parties (Eve and Mallory)
   - Goals of integrity
     - Bob is sure that *M came from Alice*
       - Notice: Need authentication!
     - Bob can *detect modifications to M*
   #+ATTR_REVEAL: :frag appear
   - Non-goals: Alice cannot be sure
     - that no third party receives M
     - that Bob receives M
     - that Bob receives M in unchanged form

# ** Message Authentication Codes (MACs)
#    - MAC = *cryptographic checksum*
#      - (Do not confuse with MAC = mandatory access control)
#    - E.g., *keyed* SHA-3
#      - Concatenate message M and *shared secret K_{AB}*: M + K_{AB}
#      - Compute hash value as MAC: *SHA-3(M + K_{AB})*
#    - Notice: No “real” signature
#      - Everyone knowing K_{AB} can produce MAC

# ** MACs
#    - “Signing” of M by Alice with hash function h and shared secret K_{AB}
#      (“+” denotes concatenation)
#      - [M]_{K_{AB}} = M + h(M + K_{AB}) = message + MAC
#    - Some message [M] *received* by Bob
#    - *Verification* whether [M] sent by Alice and unchanged along the way
#      - Split [M]: [M] = M’ + MAC’
#      - Compute MAC = h(M’ + K_{AB})
#      - Verify MAC = MAC’

** General Idea
   - Alice sends message along with its fingerprint
     - [[#jitt-hashing][Recall]]: A hash value is not good enough
     - Instead: Use some ingredient that is *unknown to the attacker*
   #+ATTR_REVEAL: :frag appear
   - Bob receives message and fingerprint and verifies whether
     both match
     - If message changed by Mallory, he cannot produce a matching
       fingerprint
   #+ATTR_REVEAL: :frag appear
   - Typical techniques
     - Message authentication codes
       - E.g., Alice and Bob share secret K_{AB}, concatenate that to message
         before hashing
     - Digital signatures (next slides)

** Digital Signatures
   :PROPERTIES:
   :CUSTOM_ID: digital-signatures
   :END:
   - Based on asymmetric cryptography
     - En- and decryption *reversed*
   #+ATTR_REVEAL: :frag appear
   - Basic idea
     - *Signature* created by encryption with *private* key: K_{A-}(M)
       - Only Alice can create this!
     - *Verification* via decryption with *public* key: D(K_{A+}, K_{A-}(M))
       - Everyone can do this as public key is public!
   #+ATTR_REVEAL: :frag appear
   - Practice: Encrypt hash value of M, e.g., K_{A-}(SHA-3(M))
     - Recall
       - Performance
       - Hash collisions

*** Some Details of Digital Signatures (1/2)
    - *Signing* of M by Alice with private key K_{A-}
      - Signature S = K_{A-}(h(M))
	- Only Alice can do this
      - Transmit signed message [M]_{K_{A-}} = M + S = message +
	signature
	- (“+” is concatenation)
    [[./img/10-signature-generate.png]]

*** Some Details of Digital Signatures (2/2)
    - [M] *received* by Bob
    - *Verification* whether [M] sent by Alice and unchanged along the way
      [[./img/10-signature-verify.png]]
      - Split [M]: [M] = M’ + S’
      - Hash M’: H = h(M’)
      - Decrypt S’: H’ = K_{A+}(S’)
	- Bob needs public key of Alice to do this
	- Everyone can do this
      - Verify H = H’

*** GnuPG: Digital Signatures
    - ~gpg --sign -a -b alice.txt~
      - Creates ~alice.txt.asc~
    - ~gpg --verify alice.txt.asc~
      - Expects to be verified content as ~alice.txt~
      - Verifies signature
      - Frequently used to verify integrity of downloads

** Electronic Signatures
    - “Signatures” of varying legal impact in IT environments
      - European Parliament and Council Directive 1999/93/EG
      - Transposed into national laws ([[https://de.wikipedia.org/wiki/Signaturgesetz_(Deutschland)][Signaturgesetz]] in Germany)
      - Three types: simple (e.g., sender’s name in e-mail), advanced
        (digital signature as discussed above), qualified
      - Qualified electronic signatures may replace paper based signatures
	(e.g., dismissal, invoice)
	- Subset of advanced electronic signatures
	- Based on qualified certificates (with qualified electronic
          signature, issued by accredited organization; law prescribes
          rules concerning infrastructure and processes)
	- Created on secure signature-creation devices
          ([[https://de.wikipedia.org/wiki/Elektronischer_Personalausweis][nPA]]
          may store qualified certificate; additional reader necessary)

** JiTT Assignments
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:

*** Asymmetric Cryptography
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:
    1. Mark the correct statements in
       [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=846536][Learnweb]].
       - If the hash function used to create a digital signature produces
         a hash collision for messages M and M’, recipients will not know
         whether M was signed or M’ or both.
       - Alice encrypts messages to Bob with her public key.
       - Alice encrypts messages to Bob with her private key.
       - Alice encrypts messages to Bob with Bob’s public key.
       - Alice encrypts messages to Bob with Bob’s private key.
       - Alice needs her public key to sign messages.
       - Alice needs her private key to sign messages.
       - Alice needs Bob’s public key to sign messages addressed to him.
       - Alice needs Bob’s private key to sign messages addressed to him.
       - Bob needs Alice’s public key to verify her signatures.
       - Bob needs Alice’s private key to verify her signatures.
       - I generated a key pair with GnuPG and sent an encrypted e-mail.

*** Preparation and Feedback
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:

    2. [@2] To prepare the in-class meeting, please bring along some colored
       pens, e.g., text markers.

    3. Answer the following
       [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=846536][question in Learnweb]]
       or Etherpad (see [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/forum/view.php?id=802190][Discussion Forum]] in Learnweb).

       {{{understandingquestion}}}

* In-Class Meeting

** Asymmetric Cryptography with Colors
   - Based on
     [[http://web.science.mq.edu.au/~len/secgame/index.html][Security Protocol Game]]
     by Len Hamey
   - [[./texts/security-color-game.pdf][Instructions]]
     - Form groups of 3
       - Every group member with a different color
     - Mark message with your color to sign
     - Place message into colored envelope to encrypt

** (In-) Security News
   - ZDF heute journal, June 28

** Basic OS Security Services

*** Service Overview (1/2)
    - Rights management, *authorization*
      - Discussed already: [[file:OS09-Processes.org::#access-rights][Access rights]]
	- What is Bob allowed to do?
    - *Logging*
      - Who did what when?
      - (Not considered in following)
    - Basic *cryptographic* services
      - Offering selection of above techniques: a/symmetric techniques, hashing

*** Service Overview (2/2)
    - *Identification/Authentication*
      - Identification: Claim of identity
	- I’m Bob …
      - Authentication: Proof of identity
	- My password is “p@ssw0rd”
	  - (Bad idea, easily broken!)
    - *Integrity protection*

*** Authentication
    - Proof of identity
      - Something the individual knows
	- Password, PIN, answers to security questions
      - Something the individual possesses
	- Smartcard, iTAN
      - Something the individual is
	- Static biometrics, e.g., fingerprint, iris scan
      - Something the individual does
	- Dynamic biometrics, e.g., voice or typing pattern
    - Necessary prerequisite to enforce [[file:OS09-Processes.org::#acess-rights][access rights]]
      - Who is allowed to perform what operation on what resource?

*** Two-Factor Authentication
    - Combinations of above categories
      - Physical banking
	- Bank card (possession) plus PIN (knowledge)
      - Online banking
	- Password for login (knowledge) plus mTAN or iTAN (possession)
      - Beware: Must keep factors separate
	- Do not record PIN on card
	- Do not perform online banking on device that receives mTAN

** Passwords
*** Authentication with Passwords
     - *Randomness* of passwords
       - Measured via entropy
       - Ca. 4 bits per character in natural language
	 - (Contrast this with 8 bits, which are necessary for typical
            representation)
       - Thus, 32 bits per 8 character password
	 - Instead of 64 bits if characters were randomly distributed
	 - 2^{32} is about a factor of 10^9 smaller than 2^{64}
     - Consequently, security of memorizable passwords is *limited*

*** Password Advice
   - *Write down* strong and diverse passwords
     - Strong: Use entire keyboard alphabet, no real words, > 10
	characters
     - Where to store?
       - Martin Hellman: “[[https://www.technologyreview.com/s/405473/calling-cryptographers/][Write down your password. Your wallet is a lot more secure than your computer.]]”
       - Attackers?

*** Password Checking
    - *Never* store passwords in *plain text*
    - Typical setup
      - System stores (salted -- next slide) *hash value* of password
	- GNU/Linux: ~/etc/shadow~
	- Windows: Security Accounts Manager (SAM) or Active Directory
           (AD)
    - Verification
      - System computes hash value for entered password string
      - Compare computed to stored value
    - (Alternative, e.g., Kerberos)
      - Use hash value of password as symmetric encryption key

*** Sample Password Attacks
    - [[https://en.wikipedia.org/wiki/Social_engineering_(security)][Social engineering]]
    - Guessing
    - Observe or record
      - Shoulder surfing, (web)cam, keylogger, network sniffing, extract
	 from RAM, deduce from cache, etc.
      - *Friend or Foe?: Your Wearable Devices Reveal Your Personal PIN*
	- [[http://dx.doi.org/10.1145/2897845.2897847]]

*** Brute Force and Salting
    - Brute force = Try “all” combinations
      - Various heuristics, implemented in tools
	- Try common patterns first
          - Databases of stolen passwords (e.g., “123456”)
          - Variations of dictionary words (e.g., “p@ssw0rd” above)
	- [[https://en.wikipedia.org/wiki/Rainbow_table][Rainbow tables]]: precomputed hash values
	- Counter measure: *Salting*
	  - Concatenate password with random number (=salt), then compute hash
	  - Store random number and hash

** Key Security Best Practices
   - Consult others
   - Adopt a holistic risk-management perspective
   - Deploy firewalls and make sure they are correctly configured
   - Deploy anti-virus software
   - Keep all your software up to date
   - Deploy an IDS
   - Assume all network communications are vulnerable
   - … (see Sec. 11.8 in cite:Hai17)

* Conclusions

** Summary
   - Security is complex, requires design and management
   - Cryptography provides foundation for lots of security mechanisms
     - Don’t implement cryptographic protocols yourselves!
     - Use proven tools, e.g., GnuPG
   - Asymmetric crypto with key pairs
     - Public key for encryption and signature verification
     - Private key for decryption and signature creation
   - Hash functions and digital signatures for integrity

** Learning Objectives
   - Explain confidentiality and integrity as security goals
     - Discuss differences between end-to-end and hop-by-hop goals
   - Explain use of hash values and digital signatures for
     integrity protection and discuss their differences

#+MACRO: copyrightyears 2017, 2018
#+INCLUDE: backmatter.org
